/**
 * Created by Irelynx on 02.05.2017.
 * https://gitlab.com/Irelynx
 * https://github.com/Irelynx
 */

// var BaseURL = 'file:///F:/www/kit45.ifmo.ru',
// var BaseURL = 'http://localhost:8000/kit45.ifmo.ru',
var BaseURL = 'http://kit45.ifmo.ru',
    BaseURL_additional = '/',
    ROOT_Catalog = 'pages/',
    DefaultExtension = '.html';
// full filePath: file:///F:/www/kit45.ifmo.ru/pages/*.html
/**
 * @desc AJAX function, prepare and send request to server at url
 * @param {string} type
 * @param {string} url
 * @param {function} callback
 */
function ajax(type, url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open(type, BaseURL + BaseURL_additional + url);
 // xhr.open('GET\POST', 'https://asdas')
    xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                callback(this.responseText);
            } else {
                console.error('Server returns a '+ this.status + ' status code. URL: "'+ BaseURL + BaseURL_additional + url +'"');
                callback('Unexpected Error. ['+ this.status +']');
            }
        }
    };
    xhr.send(null);
}

document.addEventListener('DOMContentLoaded', function(){
    if (window.location.search) {// if we have a location to load, just load it
        ajax('GET', ROOT_Catalog + window.location.search.split('?', 2)[1].split('&', 1)[0] + DefaultExtension, function(data){
            document.getElementsByTagName('main')[0].innerHTML = data;
        });
    } else {// otherwise download a default page
        ajax('GET', ROOT_Catalog + 'index.html', function(data){
            document.getElementsByTagName('main')[0].innerHTML = data;
        });
    }
    var to;
    for (var e in (to = document.querySelectorAll('[to]'))) {
        if (+e>-1) {
            try {// on each pseudo-link with attribute "to", add event "click"
                to[e].addEventListener('click', function () {
                    ajax('GET', ROOT_Catalog + this.getAttribute('to') + DefaultExtension, function (data) {
                        document.getElementsByTagName('main')[0].innerHTML = data;
                        if (document.querySelectorAll('iframe').length == 1) { // document frame
                            document.getElementsByTagName('main')[0].style.position = 'absolute';
                            document.getElementsByTagName('main')[0].style.height = 'calc(100% - 10rem)';
                        } else { // some frames on page
                            document.getElementsByTagName('main')[0].style.position = 'initial';
                            document.getElementsByTagName('main')[0].style.height = '';
                        }
                    });
                    // add new "page" to history
                    window.history.pushState({param: 'Value'}, this.innerText, '?' + this.getAttribute('to'));
                });
            } catch (err) {
                console.warn(err);
                console.warn(to[e]);
            }
        }
    }

    document.getElementById('hide-show').addEventListener('click', function(){ // show-hide navigation bar
        if (this.style.transform == 'rotate(180deg)') {
            this.style.transform = 'rotate(0deg)';
            document.getElementsByTagName('nav')[0].style.left = '0';
            document.getElementsByTagName('main')[0].style.marginLeft = '23%';
            document.getElementsByTagName('main')[0].style.width = '77%';
        } else {
            this.style.transform = 'rotate(180deg)';
            document.getElementsByTagName('nav')[0].style.left = '-19%';
            document.getElementsByTagName('main')[0].style.marginLeft = '4%';
            document.getElementsByTagName('main')[0].style.width = '96%';
        }
    });
    window.addEventListener('popstate', function(e){ // on history back/forward
        if (window.location.search) {// if we have a location to load, just load it
            ajax('GET', ROOT_Catalog + window.location.search.split('?', 2)[1].split('&', 1)[0] + DefaultExtension, function(data){
                document.getElementsByTagName('main')[0].innerHTML = data;
            });
        } else {// otherwise download a default page
            ajax('GET', ROOT_Catalog + 'index.html', function(data){
                document.getElementsByTagName('main')[0].innerHTML = data;
            });
        }
    });
});

