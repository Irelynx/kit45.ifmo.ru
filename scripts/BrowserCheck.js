/**
 * Created by Irelynx on 15.05.2017.
 * https://gitlab.com/Irelynx
 * https://github.com/Irelynx
 */
UserAgent = {
    browser : {
        platform: "",
        browser: "",
        versionFull: "",
        version: "",
        touchSupport: false
    },
    init: function(){
        var ua = navigator.userAgent,
            browser = function(){
                if (ua.search(/Edge/)>-1) return "edge";
                if (ua.search(/MSIE/)>-1) return "ie";
                if (ua.search(/Trident/)>-1) return "ie11";
                if (ua.search(/Firefox/)>-1) return "firefox";
                if (ua.search(/Opera/)>-1) return "opera";
                if (ua.search(/OPR/)>-1) return "operaWebkit";
                if (ua.search(/YaBrowser/)>-1) return "yabrowser";
                if (ua.search(/Chrome/)>-1) return "chrome";
                if (ua.search(/Safari/)>-1) return "safari";
                if (ua.search(/Maxthon/)>-1) return "maxthon";
            }(),
            version,
            touchSupport = "ontouchstart" in window,
            platform = ((/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(ua.toLowerCase()))?"mobile":"desktop");

        switch (browser) {
            case "edge":
                version = (ua.split("Edge")[1]).split("/")[1];
                break;
            case "ie":
                version = (ua.split("MSIE ")[1]).split(";")[0];
                break;
            case "ie11":
                browser = "ie";
                version = (ua.split("; rv:")[1]).split(")")[0];
                break;
            case "firefox":
                version = ua.split("Firefox/")[1];
                break;
            case "opera":
                version = ua.split("Version/")[1];
                break;
            case "operaWebkit":
                browser = "opera";
                version = ua.split("OPR/")[1];
                break;
            case "yabrowser":
                version = (ua.split("YaBrowser/")[1]).split(" ")[0];
                break;
            case "chrome":
                version = (ua.split("Chrome/")[1]).split(" ")[0];
                break;
            case "safari":
                version = (ua.split("Version/")[1]).split(" ")[0];
                break;
            case "maxthon":
                version = ua.split("Maxthon/")[1];
                break;
        }



        try {
            this.browser = {
                platform: platform,
                browser: browser,
                versionFull: version,
                versionShort: version.split(".")[0],
                touchSupport: touchSupport
            };
        } catch (err) {
            this.browser = {
                platform: platform,
                browser: "unknown",
                versionFull: "unknown",
                versionShort: "unknown",
                touchSupport: "unknown"
            };
        }
    },
    checkIntegrity: function(){
        /**
         *
         * Здесь представлен пример кода,
         * который проверяет по версии браузера
         * совместимость ресурса с ним (коэфициенты
         * версий надо указывать вручную),
         * затем, если весрия удовлетворяет требованиям,
         * удаляет сообщение в блоке warn и возвращает true,
         * если не удовлетворяет, вернет false и оставит блок warn.
         *
         * Пример warn блока
         * <html>
         * <warn>ВАШ БРАУЗЕР НЕ ИМЕЕТ ПОДДЕРЖКИ ECMA 5 ИЛИ 6, ФУНКЦИИ МОГУТ РАБОТАТЬ НЕКОРРЕКТНО!<br>ОБНОВИТЕ БРАУЗЕР ДО ПОСЛЕДНЕЙ ВЕРСИИ ИЛИ ВОСПОЛЬЗУЙТЕСЬ ИНЫМИ БРАУЗЕРАМИ, ТАКИМИ КАК GOOGLE CHROME/MOZILLA FIREFOX.<br><small>Если возникла ошибка при определении вашего браузера, и вы уверены в его возможностях, нажмите кнопку ниже</small><hr><goNext>Все равно продолжить</goNext></warn>
         * <head>
         *     ...
         *
         */

        var v = UserAgent.browser.versionShort,
            version = UserAgent.browser.versionFull,
            browser = UserAgent.browser.browser,
            b = document.getElementsByTagName("goNext")[0];
        b.onclick = deleteMessage = function(){
            p = document.getElementsByTagName("warn")[0];
            p.parentNode.removeChild(document.getElementsByTagName("warn")[0]);
        };
        // сейчас выставлены параметры, при которых браузеры 99% поддерживают HTML5 Canvas API, History API, LocalStorage и ECMA 5/6
        switch (browser) {
            case "chrome":
                if (+v > 49) deleteMessage(); return true;
                break;
            case "firefox":
                if (+v > 45) deleteMessage(); return true;
                break;
            case "edge":
                if (+v > 13) deleteMessage(); return true;
                break;
            case "safari":
                if (+v > 9) deleteMessage(); return true;
                break;
            case "opera":
                if (+v > 39) deleteMessage(); return true;
                break;
            case "yabrowser":
                if (+v > 15) deleteMessage(); return true;
                break;
            case "maxthon":
                if (+v > 3) deleteMessage(); return true;
                break;
        } return false;
    }
}
UserAgent.init();
UserAgent.checkIntegrity();