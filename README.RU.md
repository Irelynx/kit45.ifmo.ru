# :ru: Kit45.ifmo.ru

## :book: Содержание директорий

* :memo: [Index.html](./index.html) - Содержит в себе меню с навигацией и основную верстку
* :open_file_folder: [Styles](./styles) - Содержит подключаемые стили
    * :memo: [Index.css](./styles/index.css) - Основная таблица стилей
    * [FontAwesome.css](./styles/FontAwesome.css) - Настройки для шрифта [FontAwesome](http://fontawesome.io/)
    * [Roboto.css](./styles/Roboto.css) - Настройки для шрифта [Roboto](https://fonts.google.com/specimen/Roboto) от Google
* :open_file_folder: [Scripts](./scripts) - Содержит подключаемые JavaScript файлы
    * :memo: [Index.js](./scripts/index.js) - Основной используемый JavaScript код
    * [BrowserCheck.js](./scripts/BrowserCheck.js) - Код для определения версии и типа браузера
* :open_file_folder: [Images](./img) - Папка с картинками (можно хранить и в [./pages](./pages))
* :open_file_folder: [Fonts](./fonts) - Файлы шрифтов
    * [FontAwesome](./fonts/FontAwesome) - Файлы шрифта [FontAwesome](http://fontawesome.io/)
    * [Roboto](./fonts/Roboto) - Файлы шрифта [Roboto](https://fonts.google.com/specimen/Roboto)
* :open_file_folder: [Pages](./pages) - Папка со страницами
    * Other Files & Folders

## :rocket: Настройка

### [Index.js](./scripts/index.js) - Основные параметры

```javascript
/**  Основная конфигурация получения страниц

BaseURL - базовый URL сайта (по сути местоположение главного index.html)
Примеры:
file:///F:/www/kit45.ifmo.ru
http://localhost:8000/kit45.ifmo.ru

ROOT_Catalog - каталог со страницами, на конце обязателен "/"

DefaultExtension - расширение файлов страниц для открытия
**/

var BaseURL = 'http://kit45.ifmo.ru',
    BaseURL_additional = '/',
    ROOT_Catalog = 'pages/',
    DefaultExtension = '.html';

// Итоговый путь к файлам: file:///F:/www/kit45.ifmo.ru/pages/*.html
```

### [BrowserCheck.js](./scripts/BrowserCheck.js) - Ограничение доступа с определенных браузеров

```javascript
/**

Для ограничения доступа к сайту определенным браузерам, необходимо
определится с версиями браузеров, которые будем ограничивать.

версии указываются внутри if блоков, и если версия выше указанной,
то сообщение удаляется (выполнится функция deleteMessage)

Подробнее можно почитать в BrowserCheck.js[:87@checkIntegrity]

**/

switch (browser) {
    case "chrome":
        if (+v > 49) deleteMessage(); return true;
        break;
    case "firefox":
        if (+v > 45) deleteMessage(); return true;
        break;
    case "edge":
        if (+v > 13) deleteMessage(); return true;
        break;
    case "safari":
        if (+v > 9) deleteMessage(); return true;
        break;
    case "opera":
    ...
}
```

### [Index.html](./index.html) - Меню

Меню оформляется достаточно стандартно, через маркированные списки.

За исключением одного нововведения - для перехода на другие страницы к любой "ссылке", `div`-блоку, `li`-пункту и прочим элементам, необходимо добавить атрибут `to='Path/To/File'`, который даст понять, что на нажатие по элементу надо среагировать и вызвать событие, которое загрузит содержание страницы из файла `./pages/Path/To/File.html`

Рассмотрим пример:

```
<div to="Folder/File">./pages/Folder/File.html</div>
```

По нажатию по этому блоку, будет совершен `GET` запрос по следующему URL: `ROOT_Catalog + this.getAttribute('to') + DefaultExtension`, в нашем случае: `GET pages/Folder/File.html`.

Данные из файла будут записаны в блок `main` с перезаписью текущего контента.

#### Пример меню:

```html
<nav>

<ul>
    <li to="index">Главная страница</li>
    <li to="testpage">Тестовая страница 1</li>
    <li><a to="testx/index">Раздел 1</a>
        <ul>
        <li to="testx/index">Главная страница раздела 1</li>
        <li to="documenttest">Document Test</li>
        <li></li>
        <li></li>
        </ul>
    </li>
    <li></li>
    <li>
        <ul>
        <li></li>
        </ul>
    </li>
    <li></li>
</ul>

</nav>
```

## :heavy_plus_sign: Положительные стороны решения

+ Подгружаются только те данные, которые необходимы. Шрифты, стили и некоторый постоянный контент загружается единожды, и не перезапрашивается у сервера во время одной сессии
+ Полноценная поддержка истории перемещений

## :heavy_minus_sign: Отрицательные стороны решения и неудобства

- Меню **должно** быть обьявлено до полной загрузки страницы, т.е. жестко прописано в главном `index.html` и не должно менятся во время одной сессии до перезагрузки страницы. (Можно довольно просто исправить)
- Подгрузку `pdf`-документов и некоторых иных типов файлов необходимо совершать через добавление `iframe` и еще одной страницы, как это сделано в примере [DocumentTest.html](./pages/documenttest.html)
- Файлы страниц **не должны** содержать следующие теги: `html`, `head`, `body`, иначе возможны проблемы с отображением на клиенте.